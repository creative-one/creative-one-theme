import {useState} from 'react'

export default function Boilerplate({testProp = 'Prop is working'}){
    const [toggle, setToggle] = useState(false)

    return (
        <div className={'co-boilerplate'}>
            <h2>New Component <strong>Boilerplate</strong></h2>
            <p>{testProp}</p>
            <span>Toggle is <strong>{toggle ? 'true' : 'false'}</strong></span>
            <button onClick={() => setToggle(!toggle)}>Switch toggle</button>
        </div>
    )
}
