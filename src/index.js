import ReactHabitat from 'react-habitat';
import chain from 'ramda/es/chain'
import compose from 'ramda/es/compose'
import map from 'ramda/es/map'
import toPairs from 'ramda/es/toPairs'

const asyncContexts = [
    require.context('./components', true, /habitat-async\.js$/),
    require.context('./gutenberg/blocks/', true, /habitat-async\.js$/),
]

const getRegisteredComponents = compose(
    chain(context => map(key => context(key).default)(context.keys()))
)

class WpApp extends ReactHabitat.Bootstrapper {
    constructor(){
        super();

        // Create a new container builder:
        const builder = new ReactHabitat.ContainerBuilder();


        const asyncComponents = getRegisteredComponents(asyncContexts)
        const asyncComponentPairs = chain(toPairs, asyncComponents)

        // Register a component:
        asyncComponentPairs.forEach(([name, component]) =>
            builder.registerAsync(component).as(name)
        )

        // Finally, set the container:
        this.setContainer(builder.build());
    }
}

export default new WpApp();
